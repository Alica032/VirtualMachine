from utils import run_vm
import dis
import inspect
import operator
import collections
import types

Block = collections.namedtuple("Block", "type, handler, level")


class Frame(object):
    def __init__(self, code_obj, global_names, local_names, prev_frame):
        self.code_obj = code_obj

        self.f_globals = global_names
        self.f_locals = local_names
        self.prev_frame = prev_frame
        self.stack = []
        self.sequence_instructions = []

        self.jump_instr = {}

        for i, op in enumerate(dis.get_instructions(code_obj)):
            self.jump_instr[op.offset] = i
            self.sequence_instructions.append((op.opname, op.argval, op.arg))

        if prev_frame:
            self.f_builtins = prev_frame.f_builtins
        else:
            self.f_builtins = local_names['__builtins__']
            if hasattr(self.f_builtins, '__dict__'):
                self.f_builtins = self.f_builtins.__dict__

        self.last_instruction = 0
        self.block_stack = []


class Function(object):
    __slots__ = [
        'func_code', 'func_name', 'func_defaults', 'func_globals',
        'func_locals', 'func_dict', 'func_closure',
        '__name__', '__dict__', '__doc__',
        '_vm', '_func',
    ]

    def __init__(self, name, code, globs, defaults, closure, vm):
        self._vm = vm
        self.func_code = code
        self.func_name = self.__name__ = name or code.co_name
        self.func_defaults = tuple(defaults) if defaults else tuple()
        self.func_globals = globs
        self.func_locals = self._vm.frame.f_locals
        self.__dict__ = {}
        self.func_closure = closure
        self.__doc__ = code.co_consts[0] if code.co_consts else None

        kw = {
            'argdefs': self.func_defaults,
        }

        self._func = types.FunctionType(code, globs, **kw)

    def __get__(self, instance, owner):
        if instance is not None:
            return Method(instance, owner, self)
        else:
            return self

    def __call__(self, *args, **kwargs):

        callargs = inspect.getcallargs(self._func, *args, **kwargs)
        frame = self._vm.make_frame(
            self.func_code, callargs, self.func_globals, {}
        )
        retval = self._vm.run_frame(frame)

        return retval


class Method(object):
    def __init__(self, obj, _class, func):
        self.im_self = obj
        self.im_class = _class
        self.im_func = func

    def __call__(self, *args, **kwargs):
        if self.im_self is not None:
            return self.im_func(self.im_self, *args, **kwargs)
        else:
            return self.im_func(*args, **kwargs)


class VirtualMachine(object):
    def __init__(self):
        self.frames = []
        self.frame = None
        self.code_obj = None
        self.return_value = None

    def run_code(self, code):
        if isinstance(code, str):
            code = compile(code, "<s>", "exec")
        self.code_obj = code
        frame = self.make_frame(code)
        self.run_frame(frame)

    def make_frame(self, code, callargs=(), f_globals=None, f_locals=None):

        if f_globals is not None:
            f_globals = f_globals
            if f_locals is None:
                f_locals = f_globals
        elif self.frames:
            f_globals = self.frame.f_globals
            f_locals = {}
        else:
            f_globals = f_locals = {
                '__builtins__': __builtins__,
                '__name__': '__main__',
                '__doc__': None,
                '__package__': None,
            }
        f_locals.update(callargs)
        frame = Frame(code, f_globals, f_locals, self.frame)
        return frame

    def run_frame(self, frame):
        self.push_frame(frame)
        while True:
            op_name, arguments, is_pos = \
                self.frame.sequence_instructions[self.frame.last_instruction]
            self.frame.last_instruction += 1

            why = None

            try:
                op_fn = getattr(self, 'op_%s' % op_name, None)

                if op_fn is None:
                    if op_name.startswith('UNARY_'):
                        self.unaryOperator(op_name[6:])
                    elif op_name.startswith('BINARY_'):
                        self.binaryOperator(op_name[7:])
                    elif op_name.startswith('INPLACE_'):
                        self.inplaceOperator(op_name[8:])
                    else:
                        raise Exception(
                            "unsupported bytecode type: %s" % op_name
                        )
                else:
                    why = op_fn() if is_pos is None else op_fn(arguments)

            except:
                raise Exception("Error")

            while why and frame.block_stack:

                block = self.frame.block_stack[-1]
                if block.type == 'loop' and why == 'continue':
                    self.jump(self.return_value)
                    why = None
                    break

                self.pop_block()
                while len(self.frame.stack) > block.level:
                    self.pop()

                if block.type == 'loop' and why == 'break':
                    why = None
                    self.jump(block.handler)

            if why:
                break

        self.pop_frame()

        return self.return_value

    def push_frame(self, frame):
        self.frames.append(frame)
        self.frame = frame

    def pop_frame(self):
        self.frames.pop()
        if self.frames:
            self.frame = self.frames[-1]
        else:
            self.frame = None

    def top(self):
        return self.frame.stack[-1]

    def pop_block(self):
        return self.frame.block_stack.pop()

    def push_block(self, type, handler=None, level=None):
        if level is None:
            level = len(self.frame.stack)
        self.frame.block_stack.append(Block(type, handler, level))

    UNARY_OPERATORS = {
        'POSITIVE': operator.pos,
        'NEGATIVE': operator.neg,
        'NOT': operator.not_,
        'CONVERT': repr,
        'INVERT': operator.invert,
    }

    BINARY_OPERATORS = {
        'POWER': pow,
        'MULTIPLY': operator.mul,
        'FLOOR_DIVIDE': operator.floordiv,
        'TRUE_DIVIDE': operator.truediv,
        'MODULO': operator.mod,
        'ADD': operator.add,
        'SUBTRACT': operator.sub,
        'SUBSCR': operator.getitem,
        'LSHIFT': operator.lshift,
        'RSHIFT': operator.rshift,
        'AND': operator.and_,
        'XOR': operator.xor,
        'OR': operator.or_,
    }

    COMPARE_OPERATORS = {
        "<": operator.lt,
        "<=": operator.le,
        "==": operator.eq,
        "!=": operator.ne,
        ">": operator.gt,
        ">=": operator.ge,
        "in": lambda x, y: x in y,
        "not in": lambda x, y: x not in y,
        "is": lambda x, y: x is y,
        "is not": lambda x, y: x is not y,
    }

    def op_COMPARE_OP(self, op):
        x, y = self.popn(2)
        self.push(self.COMPARE_OPERATORS[op](x, y))

    def unaryOperator(self, op):
        x = self.pop()
        self.push(self.UNARY_OPERATORS[op](x))

    def binaryOperator(self, op):
        x, y = self.popn(2)
        self.push(self.BINARY_OPERATORS[op](x, y))

    def inplaceOperator(self, op):
        x, y = self.popn(2)
        self.push(self.BINARY_OPERATORS[op](x, y))

    def pop(self):
        return self.frame.stack.pop()

    def push(self, *vals):
        self.frame.stack.extend(vals)

    def popn(self, n):
        if n:
            ret = self.frame.stack[-n:]
            self.frame.stack[-n:] = []
            return ret
        else:
            return []

    def jump(self, jump):
        self.frame.last_instruction = self.frame.jump_instr[jump]

    def op_POP_TOP(self):
        self.pop()

    def op_RETURN_VALUE(self):
        self.return_value = self.pop()
        return "return"

    def op_LOAD_ATTR(self, attr):
        obj = self.pop()
        val = getattr(obj, attr)
        self.push(val)

    def op_LOAD_CONST(self, const):
        self.push(const)

    def op_LOAD_NAME(self, name):
        frame = self.frame
        if name in frame.f_locals:
            val = frame.f_locals[name]
        elif name in frame.f_globals:
            val = frame.f_globals[name]
        elif name in frame.f_builtins:
            val = frame.f_builtins[name]
        else:
            raise NameError("Error")
        self.push(val)

    def op_LOAD_FAST(self, name):
        if name in self.frame.f_locals:
            val = self.frame.f_locals[name]
            self.push(val)
        else:
            raise NameError("Error")

    def op_LOAD_GLOBAL(self, name):
        f = self.frame
        if name in f.f_globals:
            val = f.f_globals[name]
        elif name in f.f_builtins:
            val = f.f_builtins[name]
        else:
            raise NameError("Error")

        self.push(val)

    def op_STORE_NAME(self, name):
        self.frame.f_locals[name] = self.pop()

    def op_STORE_ATTR(self, name):
        value, obj = self.popn(2)
        setattr(obj, name, value)

    def op_STORE_FAST(self, name):
        self.frame.f_locals[name] = self.pop()

    def op_STORE_GLOBAL(self, name):
        self.frame.f_global[name] = self.pop()

    def op_STORE_SUBSCR(self):
        val, obj, subscr = self.popn(3)
        obj[subscr] = val

    def op_CALL_FUNCTION(self, lenPos):
        posargs = self.popn(lenPos)
        func = self.pop()
        self.push(func(*posargs))

    def op_DELETE_ATTR(self, name):
        obj = self.pop()
        delattr(obj, name)

    def op_DELETE_FAST(self, name):
        del self.frame.f_locals[name]

    def op_DELETE_GLOBAL(self, name):
        f = self.frame
        if name in f.f_globals:
            del self.frame.f_globals[name]
        elif name in f.f_builtins:
            del self.frame.f_builtins[name]
        else:
            raise NameError("Error")

    def op_DELETE_NAME(self, name):
        del self.frame.f_locals[name]

    def op_DELETE_SUBSCR(self):
        obj, index = self.popn(2)
        del obj[index]

    def op_POP_JUMP_IF_FALSE(self, jump):
        val = self.pop()
        if not val:
            self.jump(jump)

    def op_POP_JUMP_IF_TRUE(self, jump):
        val = self.pop()
        if val:
            self.jump(jump)

    def op_JUMP_FORWARD(self, jump):
        self.jump(jump)

    def op_JUMP_ABSOLUTE(self, jump):
        self.jump(jump)

    def op_JUMP_IF_FALSE_OR_POP(self, jump):
        val = self.top()
        if not val:
            self.jump(jump)
        else:
            self.pop()

    def op_JUMP_IF_TRUE_OR_POP(self, jump):
        val = self.top()
        if val:
            self.jump(jump)
        else:
            self.pop()

    def op_GET_ITER(self):
        self.push(iter(self.pop()))

    def op_FOR_ITER(self, jump):
        iterobj = self.top()
        try:
            v = next(iterobj)
            self.push(v)
        except StopIteration:
            self.pop()
            self.jump(jump)

    def op_POP_BLOCK(self):
        self.pop_block()

    def op_SETUP_LOOP(self, dest):
        self.push_block('loop', dest)

    def op_BUILD_TUPLE(self, count):
        typle_n = self.popn(count)
        self.push(tuple(typle_n))

    def op_BUILD_LIST(self, count):
        list_n = self.popn(count)
        self.push(list_n)

    def op_BUILD_SET(self, count):
        set_n = self.popn(count)
        self.push(set(set_n))

    def op_BUILD_MAP(self, count):
        a = self.popn(count * 2)
        dict_n = dict(zip(a[0::2], a[1::2]))
        self.push(dict_n)

    def byte_UNPACK_SEQUENCE(self, count):
        seq = self.pop()
        for x in reversed(seq):
            self.push(x)

    def op_DUP_TOP(self):
        self.push(self.top())

    def op_DUP_TOP_TWO(self):
        a, b = self.popn(2)
        self.push(a, b, a, b)

    def op_ROT_THREE(self):
        x3, x2, x1 = self.popn(3)
        self.push(x1, x3, x2)

    def op_STORE_ANNOTATIONS(self, name):
        self.frame.f_locals['__annotations__'][name] = self.pop()

    def op_BREAK_LOOP(self):
        return 'break'

    def op_CONTINUE_LOOP(self, dest):
        self.return_value = dest
        return 'continue'

    def op_BUILD_CONST_KEY_MAP(self, size):
        typle_n = self.pop()
        dict_n = dict(zip(typle_n, self.popn(size)))
        self.push(dict_n)

    def op_BUILD_SLICE(self, argc):
        self.push(slice(*self.popn(argc)))

    def op_BUILD_STRING(self, count):
        str_list = self.popn(count)
        self.push(''.join(str_list))

    def op_BUILD_TUPLE_UNPACK(self, count):
        typle_n = self.popn(count)
        list_el = []
        for iter_list in typle_n:
            for elem in iter_list:
                list_el.append(elem)

        self.push(tuple(list_el))

    def op_BUILD_LIST_UNPACK(self, count):
        typle_n = self.popn(count)
        list_el = []
        for iter_list in typle_n:
            for elem in iter_list:
                list_el.append(elem)

        self.push(list_el)

    def op_BUILD_SET_UNPACK(self, count):
        typle_n = self.popn(count)
        list_el = []
        for iter_list in typle_n:
            for elem in iter_list:
                list_el.append(elem)

        self.push(set(list_el))

    def op_BUILD_MAP_UNPACK(self, count):
        typle_n = self.popn(count)
        list_el = {}
        for iter_list in typle_n:
            for elem in iter_list:
                list_el[elem] = iter_list[elem]

        self.push(list_el)

    def op_BUILD_TUPLE_UNPACK_WITH_CALL(self, count):
        typle_n = self.popn(count)
        list_el = []
        for iter_list in typle_n:
            for elem in iter_list:
                list_el.append(elem)

        self.push(self.pop()(*list_el), None)

    def op_BUILD_MAP_UNPACK_WITH_CALL(self, count):
        typle_n = self.popn(count)
        list_el = {}
        for iter_list in typle_n:
            for elem in iter_list:
                list_el[elem] = iter_list[elem]

        self.push(self.pop()(**list_el), None)

    def op_CALL_FUNCTION_EX(self, flags):
        argkw = {}
        if flags & 0x01:
            argkw = self.pop()

        arg = self.pop()

        if arg is not None:
            func = self.pop()
            self.push(func(*arg, **argkw))

    def op_MAKE_FUNCTION(self, argc):
        name = self.pop()
        code = self.pop()
        defaults = [None]
        if argc:
            defaults = self.popn(argc)
        globs = self.frame.f_globals
        fn = Function(name, code, globs, *defaults, None, self)
        self.push(fn)

    def op_PRINT_EXPR(self):
        print(self.pop())

    def op_NOP(self):
        pass

    def op_IMPORT_FROM(self, name):
        import_name = self.top()
        self.push(getattr(import_name, name))

    def op_LIST_APPEND(self, count):
        val = self.pop()
        the_list = self.frame.stack[-count]
        the_list.append(val)

    def op_SET_ADD(self, count):
        val = self.pop()
        the_set = self.frame.stack[-count]
        the_set.add(val)

    def op_MAP_ADD(self, count):
        val, key = self.popn(2)
        the_map = self.frame.stack[-count]
        the_map[key] = val


if __name__ == "__main__":
    vm = VirtualMachine()
    run_vm(vm)
